SOURCES=$(wildcard src/*.cpp)
OBJS=$(SOURCES:.cpp=.o)

# compiler options : add debug information in debug mode
# optimize speed and size in release mode
ifneq (,$(findstring release,$(MAKECMDGOALS)))
   CFLAGS=-O2 -s
else
   CFLAGS=-g
endif

# linker options : OS dependant
ifeq ($(shell sh -c 'uname -s'),Linux)
   LIBFLAGS=-L. -ltcod_debug -ltcodxx_debug -Wl,-rpath=.
else
   LIBFLAGS=-Llib -ltcod-mingw-debug -static-libgcc -static-libstdc++ -mwindows
endif

debug : tuto
release : tuto

tuto : $(OBJS)
	g++ $(OBJS) -o tuto -Wall $(LIBFLAGS) $(CFLAGS)

src/main.hpp.gch : src/*.hpp
	g++ src/main.hpp -Iinclude -Wall

src/%.o : src/%.cpp src/main.hpp.gch
	g++ $< -c -o $@ -Iinclude -Wall $(CFLAGS)

clean :
	rm -f src/main.hpp.gch $(OBJS)

alltuto : tuto1 tuto2 tuto3 tuto4 tuto5 tuto6 tuto7 tuto8 tuto9 tuto10 tuto10.2 tuto11 extra3 extra5

tuto1 : src1/*pp
	time g++ src1/*.cpp -o tuto1 -Iinclude -Llib -ltcod-mingw-debug -Wall -static-libgcc -static-libstdc++ -g
tuto2 :	src2/*pp
	time g++ src2/*.cpp -o tuto2 -Iinclude -Llib -ltcod-mingw-debug -Wall -static-libgcc -static-libstdc++ -g
tuto3 :	src3/*pp
	time g++ src3/*.cpp -o tuto3 -Iinclude -Llib -ltcod-mingw-debug -Wall -static-libgcc -static-libstdc++ -g
tuto4 :	src4/*pp
	time g++ src4/*.cpp -o tuto4 -Iinclude -Llib -ltcod-mingw-debug -Wall -static-libgcc -static-libstdc++ -g
tuto5 :	src5/*pp
	time g++ src5/*.cpp -o tuto5 -Iinclude -Llib -ltcod-mingw-debug -Wall -static-libgcc -static-libstdc++ -g
tuto6 :	src6/*pp
	time g++ src6/*.cpp -o tuto6 -Iinclude -Llib -ltcod-mingw-debug -Wall -static-libgcc -static-libstdc++ -g
tuto7 :	src7/*pp
	time g++ src7/*.cpp -o tuto7 -Iinclude -Llib -ltcod-mingw-debug -Wall -static-libgcc -static-libstdc++ -g
tuto8 :	src8/*pp
	time g++ src8/*.cpp -o tuto8 -Iinclude -Llib -ltcod-mingw-debug -Wall -static-libgcc -static-libstdc++ -g
tuto9 :	src9/*pp
	time g++ src9/*.cpp -o tuto9 -Iinclude -Llib -ltcod-mingw-debug -Wall -static-libgcc -static-libstdc++ -g
tuto10 : src10/*pp	
	time g++ src10/*.cpp -o tuto10 -Iinclude -Llib -ltcod-mingw-debug -Wall -static-libgcc -static-libstdc++ -g
tuto10.2 :	src10.2/*pp
	time g++ src10.2/*.cpp -o tuto10.2 -Iinclude -Llib -ltcod-mingw-debug -Wall -static-libgcc -static-libstdc++ -g
tuto11 : src11/*pp
	time g++ src11/*.cpp -o tuto11 -Iinclude -Llib -ltcod-mingw-debug -Wall -static-libgcc -static-libstdc++ -g
extra3 : extra3/*pp
	time g++ extra3/*.cpp -o tutoe3 -Iinclude -Llib -ltcod-mingw-debug -Wall -static-libgcc -static-libstdc++ -g
extra5 : extra5/*pp
	time g++ extra5/*.cpp -o tutoe5 -Iinclude -Llib -ltcod-mingw-debug -Wall -static-libgcc -static-libstdc++ -g
