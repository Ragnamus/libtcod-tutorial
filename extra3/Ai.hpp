class Ai {
public :
	virtual void update(Actor *owner)=0;
};

// after 20 turns, the monster cannot smell the scent anymore
static const int SCENT_THRESHOLD=50;

class MonsterAi : public Ai {
public :
	MonsterAi();
	void update(Actor *owner);
protected :
	void moveOrAttack(Actor *owner, int targetx, int targety);
};

class PlayerAi : public Ai {
public :
	void update(Actor *owner);

protected :
	bool moveOrAttack(Actor *owner, int targetx, int targety);
};
