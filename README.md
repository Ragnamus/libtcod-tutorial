# README #

This is the source code for all the chapters of libtcod's C++ tutorial.

For instructions, check http://www.roguebasin.com/index.php?title=Complete_roguelike_tutorial_using_C%2B%2B_and_libtcod_-_part_1:_setting_up

# MINGW build instructions #

## Build libtcod 1.5 ##

- install mercurial
- install mingw


```
#!shell

    hg clone https://bitbucket.org/libtcod/libtcod
    hg update 1.5.x
    make -f makefiles/makefile-mingw
```


## Build the tutorial ##

    hg clone https://bitbucket.org/libtcod/tutorial

- copy the previously compiled libtcod-mingw*.dll in the tutorial/ directory


```
#!shell

    make
    ./tuto.exe
```

